import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Observer } from 'rxjs';

const headers = new HttpHeaders({
  Authorization:
    'Bearer AAIgMDc0YTU0YjM1NGM2Y2EwOWI3YWM2YmE0NTVkYzA4ODKe-FjVP8wTNqrbqZdHjIS00BT6rVfj_oagDrHVJi0Y4oD9Ve9MaKDg_Z3Xt-RL7hWiiMq2TEBJS-vyO0D6ZBhZDJFqeVZu0EoSAgOwt762PeAvFvVUpfbOJKoTAnsc0XI',
  Accept: 'application/json',
  'X-IBM-Client-Id': '074a54b354c6ca09b7ac6ba455dc0882'
});

@Injectable({
  providedIn: 'root'
})
export class DemoService {
  constructor(private http: HttpClient) {}

  public AccountInquire(amount: any): Observable<any> {
    // this.getToken();
    return this.http.post<any>(
      'https://gw.bgm.io:9445/paynet/sandbox/account-inquiry/',
      { amount },
      { headers }
    );
  }

  public PaymentTransfew(amount: any): Observable<any> {
    return this.http.post<any>(
      'https://gw.bgm.io:9445/paynet/sandbox/credit-transfer/',
      { amount },
      { headers }
    );
  }

  private getToken(): void {
    this.http.get('https://example.com').subscribe((response: any) => {
      if (response) {
        headers.set('Authorization', response.token);
      }
    });
  }
}
