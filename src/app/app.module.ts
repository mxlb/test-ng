import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRouteRoutes } from './app-route.routing';
import { MaterialModule } from './shared/mat-module/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';
import { CartComponent } from './components/cart/cart.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SummaryComponent } from './components/summary/summary.component';
import { AccountDialogComponent } from './components/account-dialog/account-dialog.component';
import { SuccessSnackbarComponent } from './components/success-snackbar/success-snackbar.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CartComponent,
    CheckoutComponent,
    SummaryComponent,
    AccountDialogComponent,
    SuccessSnackbarComponent
  ],
  imports: [
    BrowserModule,
    AppRouteRoutes,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [AccountDialogComponent, SuccessSnackbarComponent]
})
export class AppModule {}
