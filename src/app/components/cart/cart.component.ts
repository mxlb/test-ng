import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  @Input() noButton = false;

  cartBasket = [
    {
      name: 'Logitech Pro Keyboard',
      price: 99.99,
      quantity: 1,
      imgSrc: 'assets/image/keyboard.png'
    }
  ];

  constructor() {}

  ngOnInit() {}
}
