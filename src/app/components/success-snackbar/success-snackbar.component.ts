import { Component, OnInit } from '@angular/core';
import { MatSnackBarRef } from '@angular/material';

@Component({
  selector: 'app-success-snackbar',
  templateUrl: './success-snackbar.component.html',
  styleUrls: ['./success-snackbar.component.scss']
})
export class SuccessSnackbarComponent implements OnInit {
  constructor(public snackbarRef: MatSnackBarRef<SuccessSnackbarComponent>) {}

  dateVal = Date.now();

  ngOnInit() {}
}
