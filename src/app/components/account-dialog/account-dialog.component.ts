import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { SuccessSnackbarComponent } from '../success-snackbar/success-snackbar.component';

@Component({
  selector: 'app-account-dialog',
  templateUrl: './account-dialog.component.html',
  styleUrls: ['./account-dialog.component.scss']
})
export class AccountDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<AccountDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private snackbar: MatSnackBar
  ) {}

  ngOnInit() {}

  approveOnClick(): void {
    this.dialogRef.close();
    this.router.navigate(['/home']);
    this.snackbar.openFromComponent(SuccessSnackbarComponent, {
      duration: 4000,
      horizontalPosition: 'center',
      panelClass: ['custom-snackbar']
    });
  }
}
