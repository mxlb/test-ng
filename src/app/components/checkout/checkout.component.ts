import { Component, OnInit } from '@angular/core';
import { MatRadioChange, MatSelectChange, MatDialog } from '@angular/material';
import { DemoService } from 'src/app/shared/service/demo.service';
import { AccountDialogComponent } from '../account-dialog/account-dialog.component';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  showForm = false;
  showField = false;
  fieldEntry = '';
  fieldValue = '';
  showCheckout = true;
  accountData = null;

  constructor(private demoService: DemoService, private dialog: MatDialog) {}

  ngOnInit() {}

  paymentOptionChange(event: MatRadioChange): void {
    this.showForm = event.value === 'duitnow' ? true : false;
  }

  duitnowOptionChange(event: MatSelectChange): void {
    this.fieldEntry = 'Enter ' + event.value;
    this.showField = true;
  }

  promptDialog(data: any): void {
    this.dialog.open(AccountDialogComponent, {
      width: '500px',
      data
    });
  }

  placeOrderOnClick(): void {
    console.log('field value: ' + this.fieldValue);
    // this.promptDialog({
    //   creditorAcctNum: 123412341234,
    //   creditorName: 'MAX LIM'
    // });
    if (this.fieldValue) {
      this.demoService.AccountInquire(99.99).subscribe(response => {
        if (response) {
          console.log(JSON.stringify(response));
          this.promptDialog(response);
        }
      });
    }
  }
}
