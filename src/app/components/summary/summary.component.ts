import { Component, OnInit } from '@angular/core';
import { DemoService } from 'src/app/shared/service/demo.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
  constructor(private demoService: DemoService) {}

  ngOnInit() {
    
  }
}
